#!/bin/sh

ldconfig
pacman --noconfirm -Sy --force coreutils bash grep gawk file tar sed acl archlinux-keyring attr bzip2 curl e2fsprogs expat gcc-libs glibc gpgme icu keyutils krb5 libarchive libassuan libgpg-error libidn libpsl libssh2 lzo lz4 openssl pacman pacman-mirrorlist xz zlib filesystem dash
sed -i "s/^[[:space:]]*\(CheckSpace\)/#\1/" /etc/pacman.conf
cat << 'EOF' > /tmp/needs-bash
PKGIGNORE=(
    cryptsetup
    device-mapper
    dhcpcd
    iproute2
    jfsutils
    linux
    lvm2
    man-db
    man-pages
    mdadm
    nano
    netctl
    openresolv
    pciutils
    pcmciautils
    reiserfsprogs
    s-nail
    systemd-sysvcompat
    usbutils
    vi
    xfsprogs
)

BASE_PACKAGES="$(pacman -Sg base | awk 'BEGIN {ORS=" "} {print $2}')"
IFS=' ' read -r -a BASE_ARRAY <<< "$BASE_PACKAGES"
PACKAGES=($(comm -13 <(printf '%s\n' "${PKGIGNORE[@]}" | LC_ALL=C sort) <(printf '%s\n' "${BASE_ARRAY[@]}" | LC_ALL=C sort)))
LANGUAGE=en_US
TEXT_ENCODING=UTF-8

pacman -S --needed --noprogressbar --noconfirm "${PACKAGES[@]}"
set -e -u -o pipefail
pacman --noconfirm -S archlinux-keyring
pacman-key --init
pacman-key --populate archlinux
echo "${LANGUAGE}.${TEXT_ENCODING} ${TEXT_ENCODING}" >> /etc/locale.gen
echo LANG="${LANGUAGE}.${TEXT_ENCODING}" > /etc/locale.conf
locale-gen
ln -s /usr/share/zoneinfo/UTC /etc/localtime
mv /etc/resolv.conf.pacnew /etc/resolv.conf; rm /etc/resolv.conf.pacnew || true
rm /etc/pacman.d/mirrorlist.pacnew || true
echo "export TERM=xterm" >> /etc/profile
EOF
bash /tmp/needs-bash
rm /tmp/needs-bash

cat << 'EOF' > /sbin/get-new-mirrors
#!/usr/bin/env bash
set -e -u -o pipefail
echo "Finding the fastest Arch mirrors..."
pacman -S --noconfirm --needed --noprogressbar reflector
reflector --verbose -l 200 -c Canada -p http --sort rate --save /etc/pacman.d/mirrorlist
pacman -Rs reflector --noconfirm
pacman -Syy
echo "Mirrorlist updated."
EOF
chmod +x /sbin/get-new-mirrors
/sbin/get-new-mirrors

pacman -Syyu --noconfirm --noprogressbar zsh
rm -rf /usr/bin/sh
ln -s $(which zsh) /usr/bin/sh

echo "keyserver hkp://keys.gnupg.net" >> /usr/share/gnupg/gpg-conf.skel
sed -i "s,#keyserver-options auto-key-retrieve,keyserver-options auto-key-retrieve,g" /usr/share/gnupg/gpg-conf.skel
mkdir -p /etc/skel/.gnupg
cp /usr/share/gnupg/gpg-conf.skel /etc/skel/.gnupg/gpg.conf
cp /usr/share/gnupg/dirmngr-conf.skel /etc/skel/.gnupg/dirmngr.conf

cp -r $(find /etc/skel -name ".*") /root

cleanup-image
